import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kooks/models/api.dart';
import 'package:kooks/ui/text.dart';
import 'package:kooks/ui/theme.dart';
import 'package:url_launcher/url_launcher.dart';

final AssetImage background =
  new AssetImage('graphics/background.png', package: 'kooks');


class MyHomePage extends StatefulWidget {
  MyHomePage({this.title});
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  _MyHomePageState({this.title});
  String title;
  List _songs = [];
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    // Get Data from API
    this.getData('34');
  }

  @override
  Widget build(BuildContext context) {
    return new DecoratedBox(
      // Full Background Image
      decoration: new BoxDecoration(
        image: new DecorationImage(
          fit: BoxFit.cover, image: background)),
      child: _buildView());
  }

  Scaffold _buildView() {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      appBar: new AppBar(
        // App Bar
        leading: new Icon(Icons.music_note),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: new Row(
          textDirection: TextDirection.rtl,
          children: <Widget>[new Flexible(child: headText(title))],
        ),
      ),
      body: _isLoading
        ? new Center(child: loadingText('Loading...'))
        : _buildList(),
    );
  }

  Widget _buildList() {
    return new ListView.builder(
      // Song List with Animation
      itemCount: _songs == null ? 0 : _songs.length,
      itemBuilder: (BuildContext context, int i) {
        // For Each Row render SongRow Widget
        return new SongRow(i, _songs[i],
          new AnimationController(
            duration: new Duration(milliseconds: 500),
            vsync: this));
      });
  }

  Future<String> getData(String mid) async {
    setState(() {
      _isLoading = true;
    });

    print('Fetching songs...');

    var res = await http.get(
        'http://mjay.herokuapp.com/api/music/trending/$mid',
        headers: {'Accept': 'application/json'});

    setState(() {
      _songs = SongApi.fromJson(res.body).data;
      _isLoading = false;
    });

    print('Succeed!');

    return 'OK';
  }
}

class SongRow extends StatelessWidget {
  SongRow(this.i, this.song, this.animationController);

  final int i;
  final Song song;
  final AnimationController animationController;
  final List<String> menu = const <String>["Get Link"];

  void _getLink(String menu) {
    var fid = song.sid.replaceFirst(new RegExp('\/'), '_');
    var url = 'http://api.joox.com/web-fcgi-bin/web_get_songinfo?songid=${fid}';
    _launchInBrowser(url);
  }

  Future<Null> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return new FadeTransition(
      // For Fade Animation
      opacity: new CurvedAnimation(parent: animationController, curve: Curves.easeIn),
      child: new Container(
          margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          height: 75.0,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                margin: const EdgeInsets.only(right: 10.0),
                height: 75.0,
                width: 75.0,
                decoration: new BoxDecoration(
                  // Smooth Shadow behind Album Image
                  color: Colors.transparent,
                  shape: BoxShape.circle,
                  boxShadow: <BoxShadow>[
                    new BoxShadow(
                      color: KooxColors.shadow,
                      blurRadius: 10.0,
                      offset: new Offset(0.0, 3.0)
                    )
                  ]
                ),
                child: new CircleAvatar(
                  // Album Image
                  backgroundImage:
                  new CachedNetworkImageProvider(song.imgSrc),
                  backgroundColor: Theme.of(context).primaryColor,
                  child: numbText('${i+1}'),
                ),
              ),
              new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    songText(song.song),
                    artistText(song.artist),
                  ]),
              ),
              new PopupMenuButton<String>(
                // Row Action Menu
                onSelected: _getLink,
                icon: new Icon(Icons.more_vert),
                itemBuilder: (BuildContext context) {
                  return menu.map((String choice) {
                    return new PopupMenuItem<String>(
                      value: choice,
                      child: new Text(choice),
                    );
                  }).toList();
                },
              )
            ])));
  }
}