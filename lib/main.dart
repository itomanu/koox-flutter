import 'package:flutter/material.dart';
import 'package:kooks/pages/home_page.dart';

import 'ui/theme.dart' as Theme;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'KOOX',
      theme: Theme.KooxThemeData,
      home: new MyHomePage(title: 'KOOX'),
    );
  }
}
