import 'dart:convert';

class SongApi {
  final String type;
  final String name;
  final List<Song> data;
  SongApi(this.type, this.name, this.data);

  static SongApi fromJson(String sJson) {
    Map json = JSON.decode(sJson);
    List songs = new List();

    for (Map songJson in json['data']) {
      songs.add(new Song.fromJson(songJson));
    }

    return new SongApi(json['type'], json['name'], songs);
  }

}

class Song {
  final String artist;
  final String album;
  final String imgSrc;
  final String song;
  final String sid;

  Song(this.artist, this.album, this.imgSrc, this.song, this.sid);

  Song.fromJson(Map<String, dynamic> json)
    : artist = json['artist'],
      album = json['album'],
      imgSrc = json['imgSrc'],
      song = json['song'],
      sid = json['id'];
}