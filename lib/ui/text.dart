import 'package:flutter/material.dart';

Text headText(text) => new Text(text,
  style: new TextStyle(
    fontFamily: 'Bungee',
    fontWeight: FontWeight.w400
  )
);

Text songText(text) => new Text(text,
  style: new TextStyle(
    fontSize: 13.0,
    fontFamily: 'Comfortaa',
    fontWeight: FontWeight.w600
  ),
  overflow: TextOverflow.ellipsis,
);

Text artistText(text) => new Text(text,
  style: new TextStyle(
    fontSize: 11.0,
    fontFamily: 'Comfortaa',
    fontWeight: FontWeight.w200
  ),
  overflow: TextOverflow.ellipsis,
);

Text menuListText(text) => new Text(text,
    style: new TextStyle(
        fontSize: 12.0,
        fontFamily: 'Comfortaa',
        fontWeight: FontWeight.w600
    )
);

Text loadingText(text) => new Text(text,
    style: new TextStyle(
        fontSize: 12.0,
        fontFamily: 'Comfortaa',
        fontWeight: FontWeight.w600
    )
);

Text numbText(text) => new Text(text,
    style: new TextStyle(
        fontSize: 35.0,
        fontFamily: 'Megrim'

    ),
);