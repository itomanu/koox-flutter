import 'package:flutter/material.dart';

final ThemeData KooxThemeData = new ThemeData(
  primarySwatch: KooxColors.primary,
  brightness: Brightness.dark,
  primaryColor: KooxColors.primary,
  accentColor: KooxColors.grey,
  splashColor: KooxColors.primary,
);

class KooxColors {
  KooxColors._(); // this basically makes it so you can instantiate this class
  static const Color primary = Colors.purple;
  static const Color grey = Colors.grey;
  static const Color shadow = Colors.black45;

}